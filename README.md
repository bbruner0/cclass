#CObject
---

CObject is a small set of macros and functions for using fundamental object oriented concepts (OO) in the C language. 

* Classes
* Single inheritance
* Interfaces (virtual class with no implemented methods)
* Virtual methods

It's implementation is designed to be as simple as possible, making application code intuitive to develop and easier to debug. 

CObject is c99 compliant.

#Compiling
---

In the folder ./cclass/ is the core code. It can be compiled into a static library by running ```make all``` in a command line. You need to have gcc and make installed.

#Tests
---

In ./tests/ there is a unit test suite. Compile this into a static library by running ```make all``` in a command line. To run the test suite, you need to compile the code in ./main/ by running ```make all``` then ```make run``` to execute it. Compiling main will automatically compile cclass and the testing code.
